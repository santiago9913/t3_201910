package controller;

import java.util.Scanner;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.logic.MovingViolationsManager;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {
 
	private static final String ENERO_CSV = "./data/EneroOrdenado.csv";
	private static final String FEBRERO_CSV = "./data/FebreroOrdenado.csv";
	
	private MovingViolationsManagerView view;
	
	private MovingViolationsManager manager; 
	
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;
	
	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	public Controller() {
		view = new MovingViolationsManagerView();
		manager = new MovingViolationsManager();
		
		//TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
		movingViolationsStack = null;
	}
	
	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					this.loadMovingViolations();
					break;
					
				case 2:
					IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
					view.printDailyStatistics(dailyStatistics);
					break;
					
				case 3:
					view.printMensage("Ingrese el número de infracciones a buscar");
					int n = sc.nextInt();

					IStack<VOMovingViolations> violations = this.nLastAccidents(n);
					view.printMovingViolations(violations);
					break;
											
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	

	public void loadMovingViolations() 
	{
		manager.loadMovingViolations(ENERO_CSV, FEBRERO_CSV);
	}
	
	public IQueue <VODaylyStatistic> getDailyStatistics () {
		// TODO
		return manager.getDalyStatistics(); 
	}
	
	public IStack <VOMovingViolations> nLastAccidents(int n) {
		// TODO
		return manager.nLastAccidents(n); 
	}
}
