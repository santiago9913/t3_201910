package model.sort;

import java.util.Comparator;
import java.util.List;


public class Ordenador<V> 
{
	/**
	 * Ordena los elementos de la lista que llega por par�metro
	 * 
	 * @param algoritmo
	 *            el algoritmo que se desea usar para ordenar. algoritmo != null
	 * @param ascendente
	 *            la direcci�n del ordenamiento. true si es ascedente o false si
	 *            es descendente
	 * @param comparador
	 *            el criterio de comparaci�n que se usar�. comaprador != null
	 * @param elementos
	 *            la lista de elementos que se desea ordenar. elementos != null
	 */
	public void ordenar(Ordenamientos algoritmo, boolean ascendente, Comparator<V> comparador, List<V> elementos) {
		switch (algoritmo)
		{
		case BURBUJA:
			ordenarBurbuja(elementos, ascendente, comparador);
			break;
		case SELECCION:
			ordenarSeleccion(elementos, ascendente, comparador);
			break;
		case INSERCION:
			ordenarInsercion(elementos, ascendente, comparador);
			break;
		case BURBUJA_BIDERECCIONAL:
			ordenarShaker(elementos, ascendente, comparador);
			break;
		case GNOME:
			ordenarGnome(elementos, ascendente, comparador);
			break;
		}

	}

	/**
	 * Ordena la lista usando el algoritmo de inscerci�n post: la lista se
	 * encuentra ordenada
	 * 
	 * @param lista
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarInsercion(List<V> lista, boolean ascendnte, Comparator<V> comparador) 
	{
		// TODO Complete seg�n la documentaci�n

		int i = 1; 

		while(i < lista.size())
		{
			int j = i; 
			boolean pos = false; 

			while(j > 0 && !pos)
			{
				if(ascendnte)
				{
					if(comparador.compare(lista.get(j), lista.get(j-1)) < 0)
					{
						V temp = lista.get(j); 
						lista.set(j, lista.get(j-1)); 
						lista.set(j-1, temp); 
						j--;
					}
					else
					{
						pos = true; 
					}
				}
				else
				{
					if(comparador.compare(lista.get(j), lista.get(j-1)) > 0)
					{
						V temp = lista.get(j); 
						lista.set(j, lista.get(j-1)); 
						lista.set(j-1, temp); 
						j--;
					}
					else
					{
						pos = true; 
					}
				}
			}
			i++; 
		}
	}

	/**
	 * Ordena la lista usando el algoritmo de selecci�n post: la lista se
	 * encuentra ordenada
	 * 
	 * @param lista
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarSeleccion(List<V> lista, boolean ascendnte, Comparator<V> comparador) 
	{
		// TODO Complete seg�n la documentaci�n

		int i = 0; 

		while(i < lista.size() -1)
		{
			int j = i+1; 
			V menorLista = lista.get(i); 
			int indiceMenor = i; 

			while(j < lista.size())
			{
				if(ascendnte)
				{
					if(comparador.compare(lista.get(j), menorLista) < 0)
					{
						menorLista = lista.get(j); 
						indiceMenor = j; 
					}
					j++;
				}
				else
				{
					if(comparador.compare(lista.get(j), menorLista) > 0)
					{
						menorLista = lista.get(j); 
						indiceMenor = j;
					}
					j++; 
				}
			}
			if(indiceMenor != i)
			{
				V temp = lista.get(i); 
				lista.set(i, lista.get(indiceMenor));
				lista.set(indiceMenor, temp); 
			}
			i++; 
		}
	}

	/**
	 * Ordena la lista usando el algoritmo de burbuja post: la lista se
	 * encuentra ordenada
	 * 
	 * @param lista
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarBurbuja(List<V> lista, boolean ascendnte, Comparator<V> comparador)
	{
		// TODO Complete seg�n la documentaci�n

		int i = lista.size()-1; 

		while(i > 0)
		{
			int j = 0; 

			while(j < i)
			{
				if(ascendnte)
				{
					if(comparador.compare(lista.get(j), lista.get(j+1)) > 0)
					{
						V temp = lista.get(j); 
						lista.set(j, lista.get(j+1)); 
						lista.set(j+1, temp); 
					}
					j++; 
				}
				else 
				{
					if(comparador.compare(lista.get(j), lista.get(j+1)) < 0)
					{
						V temp = lista.get(j); 
						lista.set(j, lista.get(j+1)); 
						lista.set(j+1, temp); 
					}
					j++; 
				}
			}
			i--; 
		} 
	}

	/**
	 * Ordena la lista usando el algoritmo de shake (burbuja bidireccional)
	 * post: la lista se encuentra ordenada
	 * 
	 * @param lista
	 *            la lista que se desea ordenar. lsita != null
	 * @param ascendnte
	 *            indica si se debe ordenar de mamenra ascendente, de lo
	 *            contrario se ordenar� de manera descendente
	 * @param comparador
	 *            comparador de elementos tipo T que se usar� para ordenar la
	 *            lista, define el criterio de orden. comparador != null.
	 */
	private void ordenarShaker(List<V> lista, boolean ascendnte, Comparator<V> comparador) {
		// TODO Complete seg�n la documentaci�n

		int inicio = 0; 
		int fin = lista.size() -1 ; 
		boolean izqder = true; 
		int actual = 0; 

		while(inicio < fin)
		{
			if(ascendnte)
			{
				if(izqder)
				{
					if(actual < fin)
					{
						if(comparador.compare(lista.get(actual), lista.get(actual + 1)) < 0);
						{
							V temp = lista.get(actual); 
							lista.set(actual, lista.get(actual+1)); 
							lista.set(actual+1, temp); 
						}
						actual++; 
					}
					else
					{
						izqder = false; 
						fin = fin -1; 
						actual = fin; 
					}
				}
				else
				{
					if(actual > inicio)
					{
						if(comparador.compare(lista.get(actual), lista.get(actual -1)) > 0); 
						{
							V temp = lista.get(actual); 
							lista.set(actual, lista.get(actual-1)); 
							lista.set(actual-1, temp); 
						}
						actual--;
					}
					else
					{
						izqder = true; 
						inicio = inicio +1; 
						actual = inicio;
					}
				}
			}
			else if(!ascendnte)
			{
				if(izqder)
				{
					if(actual < fin)
					{
						if(comparador.compare(lista.get(actual), lista.get(actual + 1)) > 0);
						{
							V temp = lista.get(actual); 
							lista.set(actual, lista.get(actual+1)); 
							lista.set(actual+1, temp); 
						}
						actual++; 
					}
					else
					{
						izqder = false; 
						fin = fin -1; 
						actual = fin; 
					}
				}
				else
				{
					if(actual > inicio)
					{
						if(comparador.compare(lista.get(actual), lista.get(actual -1)) < 0); 
						{
							V temp = lista.get(actual); 
							lista.set(actual, lista.get(actual-1)); 
							lista.set(actual-1, temp); 
						}
						actual--;
					}
					else
					{
						izqder = true; 
						inicio = inicio +1; 
						actual = inicio;
					}
				}
			}
		}
	}


/**
 * Ordena la lista usando el algoritmo de Gnome post: la lista se encuentra
 * ordenada
 * 
 * @param lista
 *            la lista que se desea ordenar. lsita != null
 * @param ascendnte
 *            indica si se debe ordenar de mamenra ascendente, de lo
 *            contrario se ordenar� de manera descendente
 * @param comparador
 *            comparador de elementos tipo T que se usar� para ordenar la
 *            lista, define el criterio de orden. comparador != null.
 */
private void ordenarGnome(List<V> lista, boolean ascendnte, Comparator<V> comparador) 
{
	// TODO Complete seg�n la documentaci�n

	int i = 1; 

	while(i < lista.size())
	{
		if(ascendnte)
		{
			if(i == 0 || comparador.compare(lista.get(i), lista.get(i-1)) >= 0)
			{
				i++;
			}
			else
			{
				V temp = lista.get(i); 
				lista.set(i, lista.get(i-1)); 
				lista.set(i-1, temp); 
				i--; 
			}
		}
		else
		{
			if(i == 0 || comparador.compare(lista.get(i), lista.get(i-1)) <= 0)
			{
				i++;
			}
			else
			{
				V temp = lista.get(i);
				lista.set(i, lista.get(i-1));
				lista.set(i-1, temp);
				i--;
			}
		}
	}
}
}