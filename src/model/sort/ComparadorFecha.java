package model.sort;

import java.util.Comparator;

import model.vo.VODaylyStatistic;

public class ComparadorFecha implements Comparator<VODaylyStatistic>
{

	@Override
	public int compare(VODaylyStatistic o1, VODaylyStatistic o2) 
	{
		int comparacion = o1.darDia().compareToIgnoreCase(o2.darDia()); 
		
		if(comparacion > 0)
		{
			return 1;
		}
		else if(comparacion < 1)
		{
			return -1; 
		}
		else
		{
			return 0; 
		}
	}

	

	

	

}
