package model.data_structures;

import java.util.Iterator;

public class Stack <E> implements IStack<E>
{
	private NodoListaSencilla<E> ultimoNodo;
	private int cantidadElementos;
	/**
	 * Construye una stack vacio
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public Stack() 
	{
		cantidadElementos = 0;
		ultimoNodo = null;
	}
	/**
	 * Construye una stack vacio
	 * 
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public Stack( E elemento) 
	{
		cantidadElementos = 1;
		ultimoNodo = new NodoListaSencilla<E>(elemento) ;
	}
	
	@Override
	public Iterator<E> iterator() 
	{
		return new  IteradorSencillo<E> (ultimoNodo);
	}

	@Override
	public boolean isEmpty() 
	{
		return (ultimoNodo==null);
	}

	@Override
	public int size() 
	{
		return cantidadElementos;
	}

	@Override
	public void push(E pElemento)
	{
		NodoListaSencilla<E> nodo = new NodoListaSencilla<E>(pElemento);
		cantidadElementos++;
		
		if (ultimoNodo == null )
		{
			ultimoNodo = nodo;
			return;
		}
		nodo.cambiarSiguiente(ultimoNodo);
		ultimoNodo = nodo;
		
		
	}

	@Override
	public E pop() 
	{
		if ( cantidadElementos == 0 )
			throw new NullPointerException("El stack esta vacio");
		E res = ultimoNodo.darElemento();
		NodoListaSencilla<E> cortesia = ultimoNodo;
		ultimoNodo = ultimoNodo.darSiguiente();
		cortesia.cambiarSiguiente(null);
		cortesia=null;
		cantidadElementos--;
		return res;
	}

}
