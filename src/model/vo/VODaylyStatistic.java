package model.vo;

public class VODaylyStatistic
{
	private String dia; 
	
	private int accidentes, infracciones, multasTotales; 
	
	public VODaylyStatistic(String dia, int accidentes, int infracciones, int multasTotales)
	{
		this.dia = dia; 
		this.accidentes = accidentes; 
		this.infracciones = infracciones; 
		this.multasTotales = multasTotales; 
	}
	
	public String darDia()
	{
		return dia; 
	}
	
	public int darAccidentes()
	{
		return accidentes;
	}
	
	public int darInfracciones()
	{
		return infracciones;
	}
	
	public int darMultasTotales()
	{
		return multasTotales; 
	}
	
}
