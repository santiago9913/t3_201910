package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {


	private int objectId;

	private String location, ticketIssueDate, violationDescription; 

	public VOMovingViolations(int objectId, String location, String ticketIssueDate, String violationDescription) {
		this.objectId = objectId;
		this.location = location; 
		this.ticketIssueDate = ticketIssueDate; 
		this.violationDescription = violationDescription; 
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription; 
	}
}
