package model.logic;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sun.org.apache.xml.internal.resolver.helpers.Debug;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.sort.ComparadorFecha;
import model.sort.Ordenador;
import model.sort.Ordenamientos;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;

public class MovingViolationsManager extends Ordenador<String>
{

	private Queue<String> queueEnero; 

	private Stack<String> stackFebrero; 

	private Ordenador<VODaylyStatistic> ordenador; 

	private ComparadorFecha comparador; 

	public MovingViolationsManager() 
	{
		queueEnero = new Queue<String>();
		stackFebrero = new Stack<String>(); 
		ordenador = new Ordenador<VODaylyStatistic>();
		comparador = new ComparadorFecha(); 
	}

	public void loadMovingViolations(String enero, String febrero)
	{
		try {
			FileReader fileReaderEnero = new FileReader(enero);
			FileReader fileReaderFebrero = new FileReader(febrero); 
			CSVReader csvReaderEnero = new CSVReaderBuilder(fileReaderEnero).withSkipLines(1).build(); 
			CSVReader csvReaderFebrero = new CSVReaderBuilder(fileReaderFebrero).withSkipLines(1).build(); 
			List<String[]> dataEnero =  csvReaderEnero.readAll(); 
			List<String[]> dataFebrero = csvReaderFebrero.readAll(); 
			for(String[] row : dataEnero) 
			{
				for(String cell : row) 
				{
					queueEnero.enqueue(cell); 
					System.out.println(cell);
				}
				System.out.println();
			}

			for(String[] row : dataFebrero) 
			{
				for(String cell : row)
				{
					stackFebrero.push(cell);
					System.out.println(cell);
				}
				System.out.println();
			}

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	public IQueue<VODaylyStatistic> getDalyStatistics()

	{
		IQueue<VODaylyStatistic> estadisticas = new Queue<VODaylyStatistic>();

		Iterator<String> itQueue = queueEnero.iterator(); 
		Iterator<String> itStack = stackFebrero.iterator(); 
		int accidentes = 0;
		int infracciones = 0; 
		int sumaMultas = 0; 
		int i = 1; 
		int contador = 0; 
		String fechaAnterior = ""; 
		boolean creada = false; 
		boolean reinicio = false;

		while(itQueue.hasNext())
		{
			String actual = itQueue.next();
			String[] datos = actual.split(";"); 
			String fecha = datos[13];
			String[] fechaDatos = datos[13].split("/"); 
			int dia = Integer.parseInt(fechaDatos[0]); 
			int mes = Integer.parseInt(fechaDatos[1]);
			String accidente = datos[12];
			int multa = Integer.parseInt(datos[8]); 

			while(i < 31)
			{
				if(dia == 31 && mes == 01)
				{
					if(reinicio == false)
					{
						estadisticas.enqueue(new VODaylyStatistic(fechaAnterior, accidentes, infracciones, sumaMultas));
						accidentes = 0;
						infracciones = 0; 
						sumaMultas = 0; 
						reinicio = true; 
					}

					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa; 
					contador++;
					fechaAnterior = fecha;
					break; 
				}
				if(dia == i)
				{
					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa;
					fechaAnterior = fecha; 
					creada = false; 
					break; 
				}
				else if((dia == i+1) && dia != 31)
				{
					if(creada == false)
					{
						estadisticas.enqueue(new VODaylyStatistic(fechaAnterior, accidentes, infracciones, sumaMultas));
						creada = true; 
					}

					i++;

					accidentes = 0;
					infracciones = 0; 
					sumaMultas = 0; 
					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa; 
					break; 
				}
				else
				{
					break; 
				}
			}			
		}


		i = 1;
		boolean reinicia = false; 
		boolean creaAnterior = false;
		boolean crea28 = false;
		boolean crea31 = false; 

		while(itStack.hasNext())
		{

			String actual = itStack.next(); 
			String[] datos = actual.split(";"); 
			String fecha = datos[13];
			String[] fechaDatos = datos[13].split("/"); 
			int dia = Integer.parseInt(fechaDatos[0]); 
			int mes = Integer.parseInt(fechaDatos[1]);
			String accidente = datos[12];
			int multa = Integer.parseInt(datos[8]);


			while(i < 28)
			{
				if(dia == 31 && mes == 01)
				{
					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa; 
					fechaAnterior = fecha;
					break; 
				}
				else if(dia == 1 && crea31 == false)
				{
					estadisticas.enqueue(new VODaylyStatistic(fechaAnterior, accidentes, infracciones, sumaMultas));
					accidentes = 0;
					infracciones = 0; 
					sumaMultas = 0; 
					crea31 = true; 

					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa;
					fechaAnterior = fecha; 
					creaAnterior = false; 
					break; 

				}
				else if(dia == 28)
				{
					if(crea28 == false)
					{
						estadisticas.enqueue(new VODaylyStatistic(fechaAnterior, accidentes, infracciones, sumaMultas));
						accidentes = 0;
						infracciones = 0; 
						sumaMultas = 0; 
						crea28 = true; 
					}

					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa; 
					contador++;
					fechaAnterior = fecha;
					break; 
				}
				else if(dia == i)
				{
					if(reinicia = false)
					{
						estadisticas.enqueue(new VODaylyStatistic(fechaAnterior, accidentes, infracciones, sumaMultas));
						accidentes = 0;
						infracciones = 0; 
						sumaMultas = 0; 
						reinicia = true; 
					}

					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa;
					fechaAnterior = fecha; 
					creaAnterior = false; 
					break; 
				}
				else if(dia == i+1 && dia != 28)
				{
					if(creaAnterior == false)
					{
						estadisticas.enqueue(new VODaylyStatistic(fechaAnterior, accidentes, infracciones, sumaMultas));
						accidentes = 0;
						infracciones = 0; 
						sumaMultas = 0; 
						creaAnterior = true; 
					}

					i++; 

					if(accidente.equals("Yes"))
					{
						accidentes++; 
					}
					infracciones++;
					sumaMultas+= multa; 
					break; 
				}
				else
				{
					break;
				}
			}
		}

		estadisticas.enqueue(new VODaylyStatistic(fechaAnterior, accidentes, infracciones, sumaMultas));
		return estadisticas;
	}


	public Stack<String> toStack(Queue<String> lista)
	{
		Stack<String> newStack = new Stack<String>();
		Iterator<String> itLista = lista.iterator();

		while(itLista.hasNext())
		{
			String actual = itLista.next();
			newStack.push(actual);
		}

		return newStack;
	}

	public Queue<String> toQueue(Stack<String> lista)
	{
		Queue<String> newQueue = new Queue<String>();
		Iterator<String> itLista = lista.iterator();
		Stack<String> newStack = new Stack<String>();

		while(itLista.hasNext())
		{
			String actual = itLista.next();
			newStack.push(actual);
		}
		Iterator<String> itNew = newStack.iterator();
		while(itNew.hasNext())
		{
			String actual = itNew.next();
			newQueue.enqueue(actual);
		}

		return newQueue;
	}


	public IStack<VOMovingViolations> nLastAccidents(int n)
	{
		IStack<VOMovingViolations> estadisticas = new Stack<VOMovingViolations>(); 
		Stack<String> datosEnero = toStack(queueEnero); 
		Queue<String> datosFebrero = toQueue(stackFebrero); 
		Iterator<String> itFebrero = datosFebrero.iterator();
		Iterator<String> itEnero = datosEnero.iterator();
		int contador = 0;


		while(itFebrero.hasNext())
		{
			String actual = itFebrero.next(); 
			String[] info = actual.split(";"); 
			int objectId = Integer.parseInt(info[0]); 
			String location = info[2];
			String ticketIssueDate = info[13];
			String violationDescription = info[15];
			String accident = info[12]; 

			if(accident.equals("Yes"))
			{
				contador++; 
				estadisticas.push(new VOMovingViolations(objectId, location, ticketIssueDate, violationDescription));
			}

			if(contador == n)
			{
				break; 
			}
		}
		while(itEnero.hasNext())
		{
			String actual2 = itEnero.next(); 
			String[] info2 = actual2.split(";"); 
			int objectId2 = Integer.parseInt(info2[0]); 
			String location2 = info2[2];
			String ticketIssueDate2 = info2[13];
			String violationDescription2 = info2[15];
			String accident2 = info2[12]; 

			if(accident2.equals("Yes"))
			{
				contador++; 
				estadisticas.push(new VOMovingViolations(objectId2, location2, ticketIssueDate2, violationDescription2));
			}	
			if(contador == n)
			{
				break; 
			}
		}


		return estadisticas;
	}

}



